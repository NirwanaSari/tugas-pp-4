import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class JavaIO{
	static Scanner input=new Scanner(System.in);
	void copyPaste() throws IOException{
		
	
				FileInputStream in = null;
				FileOutputStream out = null;
				try {
					
					System.out.println("Tentukan lokasi file .txt yang ingin Anda salin");
					System.out.println("Contoh: ('C:/java/test/')");
					System.out.println("Jika file .txt ada di folder yang sama dengan program ini, ketik 'default");
					String inSource=input.next();
					String outSource;
					if (inSource.contentEquals("default")){
						inSource= "";
					}
					
					System.out.println("Masukan nama file .txt");
					System.out.println("Contoh: 'passage.txt'");
					String filename=input.next();
					
					do{
						System.out.println("Tentukan lokasi di mana Anda ingin menyisipkan");
						System.out.println("Contoh: ('E:/Games/')");
						outSource=input.next();
						
						if(outSource.contentEquals(inSource)){
							System.out.println("Silakan pilih jalur yang berbeda");
							continue;
						}
						break;
					}while(true);
					
				
					
					in = new FileInputStream(inSource+filename);
					out = new FileOutputStream(outSource+filename);
					int c;
					
					while ((c = in.read()) != -1) {		//-1 means end of file
						out.write(c);
					}
					System.out.println(filename+" copied succesfully to "+outSource);
					}catch(FileNotFoundException e){
						System.out.println("File tidak ditemukan");
					}finally {
						if (in != null) {
						in.close();
						}
						if (out != null) {
						out.close();
						}
					}
	
		input.nextLine();
		
	}
	
	void cutPaste() throws IOException{
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			
			System.out.println("Tentukan lokasi file .txt yang ingin Anda potong");
			System.out.println("Contoh: ('C:/java/test/')");
			System.out.println("Jika file .txt ada di folder yang sama dengan program ini, ketik 'default'");
			String inSource=input.next();
			String outSource;
			
			if (inSource.contentEquals("default")){
				inSource= "";
			}
			
			System.out.println("Harap masukan nama file .txt");
			System.out.println("Contoh: 'passage.txt'");
			String filename=input.next();
			File source = new File(inSource+filename);
			do{
				System.out.println("Tentukan lokasi di mana Anda ingin menyisipkan");
				System.out.println("Contoh: ('E:/Video/')");
				outSource=input.next();
				
				if(outSource.contentEquals(inSource)){
					System.out.println("Silakan pilih jalur yang berbeda");
					continue;
				}
				break;
			}while(true);
			
		
			
			in = new FileInputStream(inSource+filename);
			out = new FileOutputStream(outSource+filename);
			int c;
			
			while ((c = in.read()) != -1) {		//-1 means end of file
				out.write(c);
			}
			source.deleteOnExit();
			System.out.println(filename+" cut succesfully to "+outSource);
			
			
			}catch(FileNotFoundException e){
				System.out.println("File tidak ditemukan");
			}finally {
				if (in != null) {
				in.close();
				}
				if (out != null) {
				out.close();
				}
			}
		input.nextLine();
	}
	
	
	void permutation() throws IOException{
		MainMenu();
	}
	static void MainMenu() throws IOException{
		int select =-1;
		do{
			System.out.println("Main Menu");
			System.out.println("1. Permutation");
			System.out.println("2. Combination");
			System.out.println("0. Exit");
			do{
				try{
					System.out.print("Masukkan Pilihan Anda : ");
					select=input.nextInt();
					break;
					}catch(InputMismatchException e){
						System.out.println("Pilihan Salah, Coba masukkan pilihan Anda lagi");
						input.nextLine();
					}
			}while(true);
			switch(select){
			case 1:
				permutasi();
				break;
			case 2:
				kombinasi();
				break;
			case 0:
				break;
			}
		}while(select!=0);
	}	
	static void permutasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("variables.txt");
			System.out.println("-------------------------");
			System.out.println("Permutation");
			int N=0,R=0,NR;
			
			String string="";
			int reader;
			boolean negative = false;
			do{
				reader=(char)in.read();
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if (reader ==' ')break;
				if(reader!=45)string=string+String.valueOf((char)reader);
			}while(true);
		
			N =Integer.valueOf(string);
			if(negative)N=-N;
			negative=false;
			System.out.println("Nilai N : "+N);
			string="";
			
			
			if ((reader=in.read())==(-1))System.out.println("Akhir dari file");
			
			while(true){
				
				
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if(reader!=45)string=string+String.valueOf((char)reader);
				if ((reader=in.read())==-1)break;
				else continue;
				
			}
			
			R =Integer.valueOf(string);
			if(negative)R=-R;
			negative=false;
			System.out.println("Nilai R : "+R);
			if(N<R){
				throw new Exception();
			}
			int Ntotal=1;
			int NRtotal=1;
			
			
			NR=N-R;
			for(int index =1;index<=N;index++){
				Ntotal*=index;
			}
			for(int index =1;index<=NR;index++){
				NRtotal*=index;
			}
			System.out.println("Permutasi nilai N dan R : "+(Ntotal/NRtotal));
			System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan variables.txt (dengan 2 variabel di dalamnya) untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("variables.txt tidak berisi 2 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("variables.txt tidak berisi 2 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}catch(Exception e){
			System.out.println("Nilai N kurang dari nilai R");
			System.out.println("Ubah nilai yang ada di variables.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
	static void kombinasi() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("variables.txt");
			System.out.println("-------------------------");
		System.out.println("Combination");
		int N,R,NR;
		String string="";
		int reader;
		boolean negative = false;
		do{
			reader=(char)in.read();
			if (reader==45)negative=true;		
			else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
				throw new IOException();
			}
			if (reader ==' ')break;
			if(reader!=45)string=string+String.valueOf((char)reader);
		}while(true);
	
		N =Integer.valueOf(string);
		if(negative)N=-N;
		negative=false;
		System.out.println("Nilai N : "+N);
		string="";
		
		
		if ((reader=in.read())==(-1))System.out.println("Akhir dari file");
		
		while(true){
			
			
			if (reader==45)negative=true;		
			else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
				throw new IOException();
			}
			if(reader!=45)string=string+String.valueOf((char)reader);
			if ((reader=in.read())==-1)break;
			else continue;
			
		}
		
		R =Integer.valueOf(string);
		if(negative)R=-R;
		negative=false;
		System.out.println("Nilai R : "+R);
		if(N<R){
			throw new Exception();
		}
		int Ntotal=1;
		int Rtotal=1;
		int NRtotal=1;
		
		NR=N-R;
		for(int index =1;index<=N;index++){
			Ntotal*=index;
		}
		for(int index =1;index<=R;index++){
			Rtotal*=index;
		}
		for(int index =1;index<=NR;index++){
			NRtotal*=index;
		}
		System.out.println("Kombinasi nilai N dan R : "+(Ntotal/(Rtotal*NRtotal)));
		System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan variables.txt (dengan 2 variabel di dalamnya) untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("Variables.txt tidak berisi 2 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("Variables.txt tidak berisi 2 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}catch(Exception e){
			System.out.println("Nilai N kurang dari nilai R");
			System.out.println("Ubah nilai yang ada di variables.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
	
	
	
	void pascal() throws IOException{
		FileInputStream in = null;
		FileOutputStream out = null;
		
		try{
			in = new FileInputStream("pascalInput.txt");
			out = new FileOutputStream("pascalOutput.txt");
			int rows;
			int reader;
			String string="";
			while((reader=in.read()) !=-1){
				if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				string=string+String.valueOf((char)reader);
				
				if((reader=in.read()) !=-1){
					
					string=string+String.valueOf((char)reader);
				}
				
			}
			rows =Integer.valueOf(string);
			
			String newLine = System.getProperty("line.separator");
			
			int c_row,c_number;			// c_row represents the current row to print(starting from index 0)
										// c_number represents the current sequence of number to print(starting from 1)
      
	        for (c_row = 0; c_row < rows; c_row++) {				//number of rows to print
	        	
		            int spaces =(rows-c_row);					//number of spaces to print
		          
		            for (int x=0; x<spaces;x++){//printing spaces before the first number on each row
		            	string="   ";
		            	for(int index=0;index<string.length();index++){
		    	        	out.write(string.charAt(index));
		    	        }	
		    		}		              
		            long number=1;								//starting from number 1		            
		            for (c_number = 0; c_number <= c_row; c_number++){	                
		                if(number<10){
		                	string="  "+number+"   ";
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                		 	 //printing 5 spaces for 1 digit number
		                }else if(number<100){
		                	string=("  "+number+"  "); 		 //printing 4 spaces for 2 digits number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }else if(number<1000){
		                	string=(" "+number+"  ");  		 //printing 3 spaces for 3 digits number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }else{
		                	string=(" "+number+" ");  		 	 //printing 2 spaces for 4 digits or more number
		                	for(int index=0;index<string.length();index++){
			    	        	out.write(string.charAt(index));
			    	        }
		                }
		                number=number*(c_row - c_number) / (c_number + 1);			 //the pascal'string pattern
		            }
		            string=newLine;
		            for(int index=0;index<string.length();index++){
	    	        	out.write(string.charAt(index));
	    	        }
	       }
	        System.out.println("Sukses menjalankan pascalOutput.txt");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan pascalInput.txt (dengan nilai variabel 'baris') untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("pascalInput.txt tidak berisi angka");
			System.out.println("Ubah nilai yang ada di pascalInput.txt");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("pascalInput.txt tidak berisi angka");
			System.out.println("Ubah nilai yang ada di pascalInput.txt");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}
	
	void parabola() throws IOException{
		FileInputStream in = null;
		try{
			in = new FileInputStream("input_parabola.txt");
			System.out.println("-------------------------");
			System.out.println("Parabola");
			int a=0,b=0,c=0;
			
			String string="";
			int reader;
			boolean negative=false;
				
			do{
				reader=(char)in.read();
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if (reader ==' ')break;
				if(reader!=45)string=string+String.valueOf((char)reader);
			}while(true);
		
			a =Integer.valueOf(string);
			if(negative)a=-a;
			negative=false;
			

			string="";
			
			
			
			do{
				reader=(char)in.read();
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if (reader ==' ')break;
				if(reader!=45)string=string+String.valueOf((char)reader);
			}while(true);
		
			b =Integer.valueOf(string);
			if(negative)b=-b;
			negative=false;
			
		
			string="";
			
			if ((reader=in.read())==(-1))System.out.println("end of file");
			
			while(true){
				
				
				if (reader==45)negative=true;		
				else if((reader>57 && reader !=32 && reader !=-1) || (reader<48 && reader !=32 && reader !=-1)){
					throw new IOException();
				}
				if(reader!=45)string=string+String.valueOf((char)reader);
				if ((reader=in.read())==-1)break;
				else continue;
				
			}
			
			c =Integer.valueOf(string);
			if(negative)c=-c;
			negative=false;
			
			
			int D=(b*b) - (4*a*c);
			int titikBalikX=(-b/(2*a));
			int titikBalikY= D / (-4*a);
			System.out.println(a+"x^2 + "+b+"x + "+c);
			System.out.println("titik balik ("+titikBalikX+","+titikBalikY+")");
			
			if (a>0){
				System.out.println("Grafik Terbuka Ke Atas, Titik Balik Minimum");
			}
			else if (a<0){
				System.out.println("Grafik Terbuka Ke Bawah, Titik Balik Maksimum");
			}
			
			if (titikBalikX < 0) System.out.println("Titik Balik Terletak Di kiri Sumbu Y");
			else if (titikBalikX > 0) System.out.println("Titik Balik Terletak Di kanan Sumbu Y");
			else System.out.println("Titik Balik Terletak Di Sumbu Y");
			
			if (c < 0) System.out.println("Grafik Memotong Sumbu Y Di bawah Sumbu X");
			else if (c > 0) System.out.println("Grafik Memotong Sumbu Y Di atas Sumbu X");
			else System.out.println("Grafik Memotong Sumbu Y Sumbu X");
			
			
			
			System.out.println("-------------------------");
		}catch(FileNotFoundException e){
			System.out.println("\nFile tidak ditemukan");
			System.out.println("Program ini membutuhkan input_parabola.txt (dengan 3 variabel di dalamnya) untuk dijalankan");
			System.out.println("-------------------------");
		}catch(IOException e){
			System.out.println("Error.");
			System.out.println("Input_parabola.txt tidak mengandung 3 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}catch(NumberFormatException e){
			System.out.println("Error.");
			System.out.println("Input_parabola.txt tidak mengandung 3 nilai (dipisahkan oleh spasi)");
			System.out.println("-------------------------");
		}finally{
			if (in != null) {
				in.close();
				}
		}
	}
	
	public static void main(String args[]) throws IOException {
		do{
			JavaIO run = new JavaIO ();
			System.out.println("1. Copy-paste");
			System.out.println("2. Cut-paste");
			System.out.println("3. Permutation-Combination");
			System.out.println("4. Pascal Triangle");
			System.out.println("5. Parabola");
			System.out.print(":>> ");
			String choice = input.nextLine();
					
			switch(choice){
			case "1":
				run.copyPaste();
				break;
			case "2":
				run.cutPaste();
				break;
			case "3":
				run.permutation();
				break;
			case "4":
				run.pascal();
				break;
			case "5":
				run.parabola();
				break;
				
			default:
				System.out.println("Inputan salah");
				continue;
				
			}
			break;
		}while(true);
		
	}
}